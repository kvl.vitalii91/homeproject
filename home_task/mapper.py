from typing import Dict


def user_admin():
    return "User is admin"


def user_emploee():
    return "User is emploee"


data = {"user": "Vasay", "role": "emploee"}


def what_is_user(data: Dict) -> str:
    mapper_role = {"admin": user_admin, "emploee": user_emploee}
    result = mapper_role.get(data.get("role"))
    print(result())


what_is_user(data)
