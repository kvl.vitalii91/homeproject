from typing import Dict, List, Optional, Union


def count_later(text: str, later: str) -> int:
    if isinstance(text, str) and isinstance(later, str):
        return text.count(later)
    else:
        error_data = "text" if not isinstance(text, str) else "later"
        raise TypeError(f'неверный формат, {error_data} должна быть "строка"')


def main():
    string = (
        "Python is an interpreted high-level general-purpose "
        "programming language. Its design philosophy emphasizes code "
        "readability with its use of significant indentation. "
        "Its language constructs as well as its object-oriented "
        "approach aim to help programmers write clear, logical "
        "code for small and large-scale projects."
    )
    count_later(5)


if __name__ == "__main__":
    main()
# def typing(a: str, b: int, bb: List[Optional[str, int]]):
#     ...
