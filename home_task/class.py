from datetime import date


class Person(object):
    #Атребут класса
    atr_class = 'Persona'
    # конструктор
    def __init__(self, name, age):
        self.name = name
        self.age = age
        print("Создание объекта Person")
        self.__privat = 'Приватный метод'
        self._protected = 'Защищенный метод'
        self.__get_privat = "Хоп а вот и я"

    def say_hello(self):
        print("Hello")

    @classmethod
    def from_birth_year(cls, name, year):
        return cls(name, date.today().year - year)

    @property
    def privat(self):
        return self.__privat

    @privat.setter
    def privat(self, new_value):
        self.__privat = new_value

    @privat.deleter
    def privat(self):
        del self.__privat

    @staticmethod
    def live():
        return print("Я живу")

    def __repr__(self):
        return f"Person({self.name!r}, {self.age!r})"

    def __str__(self):
        return f"Name: {self.name}  Age: {self.age}"


tom = Person('Tom', 28)  # Создание объекта Person
print(tom)
person2 = Person.from_birth_year('Roark', 1994)
tom.say_hello()  # Hello
tom.new_atr = 'Новый атребут'
person2._protected ='NEW'
print(tom.new_atr)
print(person2._protected)
print(tom._Person__privat)
print(dir(tom))
print(tom.privat)
tom.privat = 'Метод Приватный'
print(tom.privat)
#del tom.privat
print(tom.privat)
